"""
merge_files.py - Merges json files in a given directory tree.

Usage:
    merge_files.py <output_file> [<source_directory>]
"""
from docopt import docopt
import json
import os


def merge(dir, output_file, has_records):
    for root, directories, files in os.walk(dir):
        for d in directories:
            has_records = merge( dir + '/' + d, output_file, has_records)
        for f in files:
            _, extension = os.path.splitext(f)
            if extension == '.json':
                with open( dir + '/' + f, 'r') as json_file:
                    for o in json.load(json_file):
                        if has_records:
                            output_file.write(',\n')
                        else:
                            has_records = True
                        output_file.write(json.dumps(o))
        return has_records


def main(args):
    try:
        with open(args['<output_file>'], 'w') as output_file:
            output_file.write('[\n')
            merge(args['<source_directory>'] if '<source_directory>' in args else '.', output_file, False)
            output_file.write('\n]')
    except Exception as e:
        print(e)


if __name__ == "__main__":
    try:
        main(docopt(__doc__))
    except Exception as e:
        print(str(e))
        exit(1)
