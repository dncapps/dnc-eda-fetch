CALL setup-environment.bat
CALL set-variables.bat
CALL %ACTIVATE_PATH%

IF "%1"=="" GOTO ALL
IF "%1"=="YESTERDAY" GOTO YESTERDAY

:RANGE
SET DATES=%1...%2
GOTO END

:YESTERDAY
SET DATES=--yesterday
GOTO END

:ALL
SET DATES=--all-dates
GOTO END

:END
python ..\eda_fetch.py %LOCATION% %RESOURCE% %SOURCE_SYSTEM% %DATES% --id=%OAUTH_CLIENT% --secret=%OAUTH_SECRET% --output=%DOWNLOAD_DIR% --json --schema=%SCHEMA%
CALL %DEACTIVATE_PATH%
