SET VENV_PATH=..\venv
SET ACTIVATE_PATH=%VENV_PATH%\Scripts\activate.bat
SET DEACTIVATE_PATH=%VENV_PATH%\Scripts\deactivate.bat
python -m venv %VENV_PATH%
CALL %ACTIVATE_PATH%
pip install -r ..\requirements.txt
CALL %DEACTIVATE_PATH%