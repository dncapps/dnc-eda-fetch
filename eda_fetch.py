"""
eda_fetch.py - Fetch data from the Delaware North Enterprise Data Archive

Usage:
    eda_fetch.py <location> [<resource> [<source_system> [<date_range> | --all-dates | --yesterday]]] [options]

Options:
    <location>                  Location id to download (e.g. mws)
    <resource>                  Type of data to download (e.g. transactions)
    <source_system>             Which source system from data to download (e.g. micros)
    <date_range>                Dates to download (can be a single date or a date range such as 2015-01-01...2015-12-31)
    --json                      Output JSON files instead of the default CSV.
    --all-dates                 Download all dates for a combination of location, resource, and source system.
    --yesterday                 Download yesterday's files for a combination of location, resource, and source system.
    --id=<id>                   OAuth client ID.
    --secret=<secret>           OAuth client secret.
    --output=<dir>              Directory to output data to.
    --schema=<schema>           The schema of the data to download. (default: sql, options: legacy, sql, sql/metadata)
    --log-level=<log-level>     The log level to report. [default: INFO]
"""
from datetime import date, datetime, timedelta

import logging

from colorlog import ColoredFormatter
from docopt import docopt
import os
import requests


class EDAInvalidParameterException(Exception):
    """Raise when the user provides a parameter that is not present in the EDA."""


class EDAException(Exception):
    pass


class EDA:
    base_path = "https://api.delawarenorth.com/dnc/eda/v1"
    oauth_path = "https://api.delawarenorth.com/oauth/client_credential/accesstoken?grant_type=client_credentials"

    def __init__(self, key, secret, json=False, schema=None):
        self.api_key = key
        self.api_secret = secret
        self.api_token = None
        self.logger = get_logger('eda_fetch.EDA', 'DEBUG')

        self.headers = {}
        self.authenticate()

        self.params = {"file_type": "csv"}
        if json:
            self.params['file_type'] = 'json'

        if schema is not None:
            self.params['schema'] = schema
        elif not json:
            logger.warning("Warning: schema option set to \'sql\' by default. To access the legacy csv files set: "
                           "'--schema=legacy'. Legeacy files will no longer be supported after 2019-02-01. "
                           "Upgrade details: "
                           "https://confluence.delawarenorth.com/pages/viewpage.action?",
                           "spaceKey=DATA&title=Upgrade+from+CSV+to+SQL+CSV+Files")
            self.params['schema'] = 'sql'

    def authenticate(self):
        payload = {"client_id": self.api_key,
                   "client_secret": self.api_secret}
        response = requests.post(self.oauth_path, payload)
        response_json = response.json()

        # If there are problems authenticating, raise an exception outlining the issue.
        if response.status_code != 200:
            if 'Error' in response_json:
                raise EDAException(response_json['Error'])
            elif 'fault' in response_json:
                raise EDAException(response_json['fault']['faultstring'])
            raise EDAException('Unknown error')
        elif 'Enterprise Data Archive' not in response_json['api_product_list']:
            raise EDAException(("You have provided credentials that do not have access to the EDA. "
                                "Please use EDA credentials."))

        self.api_token = response_json['access_token']
        self.headers = {"Authorization": "Bearer " + self.api_token}

    def get(self, location=None, resource=None, source_system=None, date=None, retry_count=0):

        path = self.base_path
        if location:
            path += "/" + location
        if resource:
            path += "/" + resource
        if source_system:
            path += "/" + source_system
        if date:
            path += "/" + date

        try:
            response = requests.get(path,
                                    params=self.params,
                                    headers=self.headers)

            if response.status_code == 200:
                response_json = response.json()

                self.verify_response(response_json)
                return response_json
            elif response.status_code == 401:
                self.authenticate()
                return self.get(location, response, source_system, date)
            elif response.status_code == 404:
                # passively skip files which are not found.
                return {}
            else:
                raise EDAException(response.json()['message'])
        except:
            if retry_count > 2:
                raise
            return self.get(location, resource, source_system, date, retry_count + 1)

    def verify_response(self, response_json):
        if 'errorMessage' in response_json:
            raise EDAException(response_json['errorMessage'])


def get_logger(name, log_level):
    logger = logging.getLogger(name)
    log_level_map = {'DEBUG': logging.DEBUG,
                     'INFO': logging.INFO,
                     'WARNING': logging.WARNING,
                     'ERROR': logging.ERROR,
                     'CRITICAL': logging.CRITICAL}
    logger.setLevel(log_level_map[log_level])
    formatter = ColoredFormatter(
        "\033[0;2m%(log_color)s>> %(asctime)s %(levelname)-8s%(reset)s\033[0;2m - %(name)s - %(message)s\033[0;0m",
        datefmt=None,
        reset=True,
        log_colors={
            'DEBUG': 'white',
            'INFO': 'cyan',
            'WARNING': 'yellow',
            'ERROR': 'red',
            'CRITICAL': 'red',
        }
    )
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def main(args):
    location = args["<location>"]
    resource = args["<resource>"]
    source_system = args["<source_system>"]
    date_range = args["<date_range>"]
    directory = args["--output"]

    eda = EDA(key=args["--id"], secret=args["--secret"], json=args['--json'], schema=args['--schema'])

    # If the user does not provide a date, use the UI to aid in data exploration.
    if not date_range:
        # Print the UI based on the data provided by the user.

        # Print the given location.
        print("Location: {location}".format(location=location))

        # Print the given resource, or a list of the available resources.
        if args["<resource>"]:
            print("Resource: {resource}".format(resource=resource))
        else:
            response = eda.get(location, None, None, None)
            try:
                print("Resources:\n\t" + "\n\t".join(response['resources']))
                exit()
            except KeyError:
                raise EDAInvalidParameterException('"{location}" is not a valid location code.'.format(
                    location=location))

        # Print the given source system if provided.
        if args["<source_system>"]:
            print("Source System: {source_system}".format(source_system=source_system))
            response = eda.get(location, resource, source_system, None)

            # If --all-dates is not set, print a list of the available dates.
            if args["--yesterday"]:
                yesterday = str((datetime.now() + timedelta(days=-1)).date())
                if yesterday in response["dates"]:
                    print("Date: " + yesterday)
                    print("Writing files:")
                    prefix = location + "_" + resource + "_" + source_system + "_" + yesterday
                    file_list = eda.get(location, resource, source_system, yesterday).items()
                    for file_name, url in file_list:
                        download_file(file_name, prefix, url, directory)
                else:
                    print("Yesterday ({}) not available.".format(yesterday))
            elif args["--all-dates"]:
                # If --all-dates is set, download all dates for the given archive path.
                print("Dates:\n\t" + "\n\tAll dates")

                if len(response.get('dates', [])) == 0:
                    raise EDAException('No {source_system} {resource} records were found for {location}.'.
                                       format(source_system=source_system,
                                              resource=resource,
                                              location=location))

                print("Writing files:")

                for meaningful_date in response["dates"]:
                    prefix = location + "_" + resource + "_" + source_system + "_" + meaningful_date
                    file_list = eda.get(location, resource, source_system, meaningful_date).items()
                    for file_name, url in file_list:
                        download_file(file_name, prefix, url, directory)
            else:
                try:
                    print("Dates:\n\t" + "\n\t".join(response['dates']))
                except KeyError:
                    raise EDAInvalidParameterException('"{source_system}" is not a valid source system for the '
                                                       '"{resource}" resource at location code "{location}".'.format(
                                                        location=location,
                                                        resource=resource,
                                                        source_system=source_system))

        else:
            # If the source system is not provided, print a list of the available source systems.
            response = eda.get(location, resource, None, None)

            try:
                print("Source Systems:\n\t" + "\n\t".join(response['source_systems']))
            except KeyError:
                raise EDAInvalidParameterException('"{resource}" is not a valid resource for the location code'
                                                   ' "{location}".'.format(location=location, resource=resource))

    # If the date or date range is provided, download the archived files.
    else:
        if '...' in date_range:
            date_from, date_to = date_range.split('...', 2)
            start_date = date(*map(int, date_from.split('-')))
            end_date = date(*map(int, date_to.split('-')))

            # Sanity check the start and end date. If the start date follows the end date then flip them out of
            # convenience to the user and inform them by console output the argument is incorrectly formatted.

            if start_date > end_date:
                temp_date = start_date
                start_date = end_date
                end_date = temp_date
                logger.warning('Date range is not in chronological order. '
                               'Changing start date = {}, end date = {}'.format(start_date, end_date))

            if start_date == end_date:
                dates = [start_date]
            else:
                dates = [start_date + timedelta(n) for n in range(int((end_date - start_date).days) + 1)]
        else:
            try:
                dates = [date(*map(int, date_range.split('-')))]
            except ValueError:
                raise EDAInvalidParameterException(
                    'The value "{date}" is not a valid date of the format YYYY-MM-DD.'.format(
                        date=date_range
                    ))

        logger.info("Searching for record for the dates: " + ", ".join(map(str, dates)))

        try:
            valid_dates = eda.get(location, resource, source_system, None)["dates"]
        except KeyError:
            raise EDAInvalidParameterException('"{location}" is not a valid location code.'.format(location=location))

        meaningful_dates = [meaningful_date for meaningful_date in dates if str(meaningful_date) in valid_dates]

        if len(meaningful_dates) == 0:
            temporal_syntax = 'on' if len(valid_dates) == 1 else 'in the interval'
            raise EDAException('No {source_system} {resource} records were found for {location} {temporal_syntax} '
                               '{date}'.format(source_system=source_system,
                                               resource=resource,
                                               location=location,
                                               date=date_range,
                                               temporal_syntax=temporal_syntax))

        logger.info("Writing files to the directory {directory}.".format(directory=directory))
        for meaningful_date in meaningful_dates:
            prefix = location.replace(":", "_") + "_" + resource + "_" + source_system + "_" + str(meaningful_date)
            file_list = eda.get(location, resource, source_system, str(meaningful_date)).items()
            for file_name, url in file_list:
                download_file(file_name, prefix, url, directory)


def download_file(file_name, prefix, url, directory, retries=0, max_retries=3):
    """
    Download an archive file from S3
    :param directory: The directory that files will be downloaded to.
    :param file_name: The name of the S3 file to download.
    :param prefix: The prefix with which to name the downloaded file.
    :param url: The pre-signed URL with which to download the file.
    :param retries: The number of times that the download_file method has been called.
    :param max_retries: The maximum number of times that the download_file method will retry downloading the file.
    """

    if directory:
        # Verify that the dir exists. If not, create it.
        if not os.path.exists(directory):
            os.makedirs(directory)
    else:
        directory = '.'

    logger.info("Downloading {url} to {directory}.".format(
        url=url,
        directory=directory
    ))

    response = requests.get(url, stream=True)
    # replace ":" from multi-tenant locations, the location:tenant syntax causes issues with file names.
    file_path = directory + "/" + prefix.replace(":", "_") + "_" + file_name
    with open(file_path, "wb") as file:
        for index, chunk in enumerate(response.iter_content(chunk_size=1024 * 1024)):
            logger.debug("Downloading chunk {index} of {file_name}.".format(
                index=index, file_name=file_path
            ))
            if chunk:  # filter out keep-alive new chunks
                file.write(chunk)
                file.flush()

    # DATA-1014: This is a content-length check to ensure that the file has downloaded completely. This length check
    # will compare the Content-Length header from S3 with the length of the file written. In the event of a mismatch,
    # the failure will be logged and we will try to download it again. We will retry 3 times.

    if retries >= max_retries:
        logger.error("Maximum retry limit reached (3). Download failed.")
        return

    file_size = os.path.getsize(file_path)
    if file_size != int(response.headers['Content-Length']):
        logger.warning("The file {file_name} does not have the expected length (Expected {expected_length} bytes, "
                       "observed {observed_length} bytes). Attempting a retry.".format(
                        file_name=file_path,
                        expected_length=response.headers['Content-Length'],
                        observed_length=file_size))

        # Retry the download. Be sure to increment the retries count.
        download_file(file_name, prefix.replace(":", "_"), url, directory, retries + 1, max_retries)


if __name__ == "__main__":
    in_args = docopt(__doc__)
    logger = get_logger('eda_fetch', in_args['--log-level'])
    try:
        main(in_args)
    except EDAException as e:
        print(str(e))
        exit(1)
