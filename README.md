# EDA Fetch

EDA Fetch is a python-based script to query and download data from the
Delaware North Enterprise Data Archive.

## Setup

This tool requires Python 3.4+ to be installed. For computers running
Windows, you can download python from 
[python.org/downloads](https://www.python.org/downloads/release/python-352/).
When installing, be sure to select the "Add Python to your PATH 
environment variable."

### Windows Installation
In a terminal, navigate to the location of the EDA Fetch script
```
$ dir /path/to/eda/fetch/script/
```

Now, navigate to the ./batch directory.
```
$ dir batch/
```

We can now install the a virtual environment and all of our dependencies using
the `setup-environment.bat` script.
```
setup-environment.bat
```

You should now be ready to use the EDA fetch script.

### *nix Installation

In a terminal, navigate to the location of the EDA Fetch script
```
$ cd /path/to/eda/fetch/script/
```

To keep the python requirements for this script contained, we will create
a virtual environment. First, let's install virtualenv, our virtual
environment handler.
```
$ pip install virtualenv
```

Now, let's create a virtualenv. We will be storing the virtual environment
in the folder `venv`, and we want to make sure that the environment is
using python 3.
```
$ virtualenv venv --python=python3
```

With the virtual environment created, let's activate it. On linux, run
```
$ source venv/bin/activate
```

or on Windows run
```
$ .\venv\Scripts\activate.bat
```

We can now install the requirements for the EDA Fetch script.
```
$ pip install -r requirements.txt
```

You should now be ready to use the EDA fetch script.

## Usage

```
eda_fetch.py - Fetch data from the Delaware North Enterprise Data Archive

Usage:
    eda_fetch.py <location> [<resource> [<source_system> [<date_range> | --all-dates | --yesterday]]] --id=<id> --secret=<secret> [--json --output=<dir>] --schema=<schema>

Options:
    <location>         Location id to download (e.g. mws)
    <resource>         Type of data to download (e.g. transactions)
    <source_system>    Which source system from data to download (e.g. foo)
    <date_range>       Dates to download (can be a single date or a date range such as 2015-01-01...2015-12-31)
    --json             Output JSON files instead of the default CSV.
    --all-dates        Download all dates for a combination of location, resource, and source system.
    --yesterday        Download yesterday's files for a combination of location, resource, and source system.
    --id=<id>          OAuth client ID.
    --secret=<secret>  OAuth client secret.
    --output=<dir>     Directory to output data to.
    --schema=<schema>  The schema of the data to download (Specific to CSV files. Can be legacy, sql, or sql/metadata. Defaults to sql. Legacy will no longer be available after 2019-02-01. Upgrade details: https://confluence.delawarenorth.com/pages/viewpage.action?spaceKey=DATA&title=Upgrade+from+CSV+to+SQL+CSV+Files).
```

## Examples
Let's see what source systems the EDA has for transactions at Aaron's Aerospace 
Academy. We can use the information we have to query for the source systems.

```
$ python eda_fetch.py aaa transactions  --id <ID> --secret <SECRET>
Location: aaa
Resource: transactions
Source Systems:
	foo
	bar
```

This means that there are two source systems that have provided data to
the EDA. Let's download all foo transactions at Aaron's Aerospace Academy
from 2014-09-26.

```
$ python eda_fetch.py aaa transactions foo 2014-09-26 --id <ID> --secret <SECRET>
Location: aaa
Resource: transactions
Source Systems: foo
Date: 2014-09-26
Writing files:
	aaa_transactions_foo_2014-09-26_transactions_sale_items.csv
	aaa_transactions_foo_2014-09-26_transactions_tax_items.csv
	aaa_transactions_foo_2014-09-26_transactions.csv
	aaa_transactions_foo_2014-09-26_transactions_fee_items.csv
	aaa_transactions_foo_2014-09-26_transactions_tender_items.csv
	aaa_transactions_foo_2014-09-26_transactions_sale_items_categories.csv
	aaa_transactions_foo_2014-09-26_transactions_price_modifiers.csv
```

If I wanted the data in a hierarchical JSON format, I could instead use
```
$ python eda_fetch.py aaa transactions foo 2014-09-26 --id <ID> --secret <SECRET> --json
Location: aaa
Resource: transactions
Source Systems: foo
Date: 2014-09-26
Writing files:
	aaa_transactions_foo_2014-09-26_transactions.json
```

In the case we want all foo transactions for Aaron's Aerospace Academy between
2014-09-26, and 2015-09-26, we could use an inclusive date range of the format 
`yyyy-mm-dd...yyyy-mm-dd`. In the end, we would get something like the following


```
$ python eda_fetch.py aaa transactions foo 2014-09-26...2015-09-26 --id <ID> --secret <SECRET>
Location: aaa
Resource: transactions
Source Systems: foo
Date: 2014-09-26
Writing files:
    aaa_transactions_foo_2014-09-26_transactions_sale_items.csv
    aaa_transactions_foo_2014-09-26_transactions_tax_items.csv
    aaa_transactions_foo_2014-09-26_transactions.csv
    aaa_transactions_foo_2014-09-26_transactions_fee_items.csv
    aaa_transactions_foo_2014-09-26_transactions_tender_items.csv
    aaa_transactions_foo_2014-09-26_transactions_sale_items_categories.csv
    aaa_transactions_foo_2014-09-26_transactions_price_modifiers.csv
    ...
    aaa_transactions_foo_2015-09-26_transactions_sale_items.csv
    aaa_transactions_foo_2015-09-26_transactions_tax_items.csv
    aaa_transactions_foo_2015-09-26_transactions.csv
    aaa_transactions_foo_2015-09-26_transactions_fee_items.csv
    aaa_transactions_foo_2015-09-26_transactions_tender_items.csv
    aaa_transactions_foo_2015-09-26_transactions_sale_items_categories.csv
    aaa_transactions_foo_2015-09-26_transactions_price_modifiers.csv
```


## Windows Batch Files

A following Windows batch files are available in the '/batch' directory which provide a more
convenient way to use the EDA fetch script:

1. download-historical.bat
1. download-yesterday.bat
1. merge-files.bat
1. set-variables.bat
1. setup-environment.bat

It is assumed Python 3.x for Windows is already installed. Before the scripts can be used the
environment variables in the 'set-variables.bat' script must edited to include the OAuth
credentials and the desired location, source system and EDC resource files to pull from EDA.

1. OAUTH_CLIENT - OAuth client ID
1. OAUTH_SECRET - OAuth secret
1. LOCATION - EDC location ID, ex. 'aaa'
1. RESOURCE - EDC resource name, ex. 'transactions'
1. SOURCE_SYSTEM - Source system name as used by EDC. ex. 'foo'
1. DOWNLOAD_DIR - Directory to download file to.
1. MERGE_FILE_NAME - Name of the merged file if the 'merge-files.bat' script is used.

The 'setup-environment.bat' script creates a python virtual environment and downloads
external libraries the 'eda_fetch.py' script requires.

The 'download-historical.bat' depends on the 'set-variables.bat' and 'setup-environment.bat'
scripts. In turn this script calls the 'eda_fetch.py' script filling in the required command
line arguments which were setup in the 'set-variables.bat' file. The 'download-historical.bat'
script has three command line options:

1. None - Defaults to downloading all available files in EDA.
1. Start and end date in YYYY-MM-DD format - Pulls down all files for the given date range.
2. Yesterday - Pulls down files for the prior day when the --YESTERDAY option is present.

By default the batch file download JSON source files. The 'download-historical.bat' script can
be edited to remove the --json parameter passed to the eda_fetch.py script if CSV downloads
are preferred over JSON.

The 'download-yesterday.bat' script is a convenience script to download yesterday's files. The
script is dependent on the 'download-historical.bat' script.

The 'merge-files.bat' script merges all files in the download directory specified in the
'set-variables.bat' files into a single file. The combined file is named per the MERGE_FILE_NAME
environment variable set from the variables batch file.

In typical use, a one time manual execution of the 'download-historical.bat' file is used to
download the bulk of historical files from EDA. Ongoing downloads can be accomplished by scheduling
execution of the 'download-yesterday.bat' script using the Windows Scheduler.

NOTE: Downloading large datasets stalls or will timeout with a permissions error. The workaround is to
download date ranges instead until all the desired data has been downloaded.